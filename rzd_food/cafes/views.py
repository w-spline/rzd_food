import datetime

from rest_framework.response import Response
from rest_framework.views import APIView

from trains.models import Station, Train, Train_route
from trains.serializers import Train_routeSerializer

from .models import Cafe, Food
from .serializers import CafeSerializer


class Cafe_listAPIView(APIView):
    """
    List all cafes in database.
    """
    def get(self, request, format=None):
        cafes = Cafe.objects.all()
        serializer = CafeSerializer(cafes, many=True)
        return Response(serializer.data)


class Cafes_by_trainAPIView(APIView):
    """
    List cafes by train_id.
    """
    def get(self, request, train_id, format=None):
        train_time = request.query_params.get('date', None)
        print(train_time)
        train_info = Train.objects.get(pk=train_id)

        # stations_with_eat = Train_route.objects.filter(train=train_id).values_list('pk', flat=True)
        stations_with_eat = Train_route.objects.filter(train=train_id)
        stats = stations_with_eat.values_list('pk')
        stations_list = []
        for stations in stations_with_eat:
            timedelta = datetime.timedelta(minutes=stations.train_road_time)
            tmp_datetime = datetime.datetime.combine(datetime.date(1, 1, 1), train_info.start_time)
            est_time = (tmp_datetime + timedelta).time()
            stations_list.append({'pk': stations.pk, 'est_time': est_time})
        print(stations_list)

        cafes_in_stations = Cafe.objects.filter(station__in=stats)
        for cafes in cafes_in_stations:
            cafes.delivery_time = '2 часа'
            cafes.station_name = 'Орёл'

        serializer = CafeSerializer(cafes_in_stations, many=True)
        return Response(serializer.data)
