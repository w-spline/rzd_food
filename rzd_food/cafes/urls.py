from django.urls import path
from .views import Cafe_listAPIView, Cafes_by_trainAPIView

urlpatterns = [
    path('', Cafe_listAPIView.as_view(), name='cafes-list'),
    path('/by_train/<int:train_id>/', Cafes_by_trainAPIView.as_view(), name='cafes_by_train-list')
]
