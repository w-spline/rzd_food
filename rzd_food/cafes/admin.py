from django.contrib import admin
from django.utils.html import format_html
from .models import Cafe, Food


class CafeAdmin(admin.ModelAdmin):
    """
    Settings for Cafe section in admin.
    """
    list_display = ('id', 'cafe_logo_img', 'name', 'min_price', 'active')
    list_display_links = ('name',)
    list_editable = ('active',)
    search_fields = ('name', 'min_price')
    readonly_fields = ['cafe_logo_img']

    def cafe_logo_img(self, obj):
        try:
            return format_html('<img src="{url}" width="30px" height="30px" />',
                               url=obj.logo.url)
        except:
            return 'no logo'
    cafe_logo_img.short_description = 'logo'


class FoodAdmin(admin.ModelAdmin):
    """
    Settings for Food section in admin.
    """
    list_display = ('id', 'active', 'name', 'cafe', 'station', 'description', 'price')
    list_editable = ('active',)
    list_display_links = ('name', 'description')
    search_fields = ('name', 'description', 'price')
    list_filter = ('station', 'cafe')


admin.site.register(Cafe, CafeAdmin)
admin.site.register(Food, FoodAdmin)
