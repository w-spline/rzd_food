from rest_framework import serializers
from .models import Cafe, Food
from trains.serializers import StationSerializer

'''
class CafeSerializer(serializers.ModelSerializer):
    """
    Serializer for cafes.
    """
    # station = StationSerializer(read_only=True, many=True)

    class Meta:
        model = Cafe
        exclude = ['station']
'''


class CafeSerializer(serializers.Serializer):
    """
    Serializer for cafes.
    """

    id = serializers.IntegerField(read_only=True)
    station_name = serializers.CharField(read_only=True)
    name = serializers.CharField()
    description = serializers.CharField()
    logo = serializers.ImageField()
    image = serializers.ImageField()
    min_price = serializers.DecimalField(decimal_places=2, max_digits=8)
    active = serializers.BooleanField()
    delivery_time = serializers.CharField()


class FoodSerializer(serializers.ModelSerializer):
    """
    Serializer for food.
    """
    class Meta:
        model = Food
        fields = '__all__'
