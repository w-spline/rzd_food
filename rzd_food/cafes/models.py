from django.db import models
from django.utils.translation import ugettext_lazy as _
from trains.models import Station


class Cafe(models.Model):
    """
    Model for Cafe objects
    """
    station = models.ManyToManyField('trains.Station', related_name='cafe',
                                     verbose_name=_('Станция'))
    name = models.CharField(max_length=120, verbose_name=_('Название'))
    description = models.TextField(blank=True, null=True, verbose_name=_('Описание'))
    logo = models.ImageField(blank=True, null=True, verbose_name=_('Логотип'))
    image = models.ImageField(blank=True, null=True, verbose_name=_('Изображение'))
    min_price = models.DecimalField(decimal_places=2, max_digits=8, verbose_name=_('Цена от'))
    active = models.BooleanField(default=True, verbose_name=_('Активно'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('кафе')
        verbose_name_plural = _('кафе')


class Food(models.Model):
    """
    Model for food objects (cafe's menu)
    """
    cafe = models.ForeignKey(Cafe, on_delete=models.CASCADE, related_name='foods',
                             verbose_name=_('Поставщик'))
    station = models.ForeignKey(Station, on_delete=models.CASCADE, related_name='foods',
                                verbose_name=_('Станция'), blank=True, null=True)
    name = models.CharField(max_length=120, verbose_name=_('Блюдо'))
    description = models.TextField(blank=True, null=True, verbose_name=_('Описание'))
    price = models.DecimalField(decimal_places=2, max_digits=8, verbose_name=_('Цена, руб.'))
    image = models.ImageField(blank=True, null=True, verbose_name=_('Фото'))
    active = models.BooleanField(default=True, verbose_name=_('Доступно'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('блюдо')
        verbose_name_plural = _('блюда')
