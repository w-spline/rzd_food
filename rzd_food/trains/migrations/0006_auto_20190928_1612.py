# Generated by Django 2.2.5 on 2019-09-28 13:12

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trains', '0005_auto_20190928_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='train_route',
            name='road_time',
            field=models.DurationField(default=datetime.timedelta(seconds=600), verbose_name='Время в пути'),
        ),
    ]
