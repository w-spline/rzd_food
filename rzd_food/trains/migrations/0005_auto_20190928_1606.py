# Generated by Django 2.2.5 on 2019-09-28 13:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trains', '0004_auto_20190928_1501'),
    ]

    operations = [
        migrations.RenameField(
            model_name='train_route',
            old_name='arrival_time',
            new_name='road_time',
        ),
    ]
