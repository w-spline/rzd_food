from datetime import datetime

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Station, Train, Train_route


class TrainAdmin(admin.ModelAdmin):
    """
    Settings for Train section in admin.
    """
    list_display = ('id', 'name', 'description', 'start_time')
    list_display_links = ('name', 'description')
    search_fields = ('name', 'description', 'start_time')


class StationAdmin(admin.ModelAdmin):
    """
    Settings for Station section in admin.
    """
    list_display = ('id', 'name', 'description')
    list_display_links = ('name', 'description')
    search_fields = ('name', 'description')


class Train_routeAdmin(admin.ModelAdmin):
    """
    Settings for approved for food order stations section in admin.
    """
    list_display = ('train', 'station', 'train_road_time', 'stop_time')
    list_display_links = ('train', 'station')
    search_fields = ('train', 'station', 'stop_time')
    list_filter = ('train', 'station')


admin.site.register(Train, TrainAdmin)
admin.site.register(Station, StationAdmin)
admin.site.register(Train_route, Train_routeAdmin)
