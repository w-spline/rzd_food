from django.db import models
from django.utils.translation import ugettext_lazy as _


class Station(models.Model):
    """
    Model for Station objects
    """
    name = models.CharField(max_length=120, verbose_name=_('Станция'))
    description = models.TextField(blank=True, null=True, verbose_name=_('Описание'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('станция')
        verbose_name_plural = _('станции')


class Train(models.Model):
    """
    Model for Train objects
    """
    name = models.CharField(max_length=120, verbose_name=_('Название/номер'))
    description = models.TextField(blank=True, null=True, verbose_name=_('Подробно'))
    start_time = models.TimeField(verbose_name=_('Отпр-е от нач.станции'), null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('поезд')
        verbose_name_plural = _('поезда')


class Train_route(models.Model):
    """
    Model for approved for food order stations
    """
    train = models.ForeignKey(Train, on_delete=models.CASCADE, verbose_name=_('Поезд'))
    station = models.ForeignKey(Station, on_delete=models.CASCADE, verbose_name=_('Станция'))
    train_road_time = models.PositiveSmallIntegerField(default=30,
                                                       verbose_name=_('Время от начала, мин.'))
    stop_time = models.PositiveSmallIntegerField(default=10, verbose_name=_('Стоянка, мин.'))

    def __str__(self):
        return f'{self.train} {self.station} через {self.train_road_time} минут'

    class Meta:
        verbose_name = _('остановки')
        verbose_name_plural = _('остановки')
