from rest_framework import serializers
from .models import Train, Station, Train_route


class StationSerializer(serializers.ModelSerializer):
    """
    Serializer for stations.
    """
    class Meta:
        model = Station
        fields = '__all__'


class TrainSerializer(serializers.ModelSerializer):
    """
    Serializer for trains.
    """
    class Meta:
        model = Train
        fields = '__all__'


class Train_routeSerializer(serializers.ModelSerializer):
    """
    Serializer for train routes.
    """
    station = StationSerializer(read_only=True)
    train = StationSerializer(read_only=True)

    class Meta:
        model = Train_route
        fields = '__all__'
