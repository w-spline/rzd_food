from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Train_route
from .serializers import Train_routeSerializer


class Train_routeAPIView(APIView):
    """
    List all trains-route in database.
    """
    def get(self, request, format=None):
        train_route = Train_route.objects.all()
        serializer = Train_routeSerializer(train_route, many=True)
        return Response(serializer.data)
