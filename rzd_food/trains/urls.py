from django.urls import path
from .views import Train_routeAPIView

urlpatterns = [
    path('routes/', Train_routeAPIView.as_view(), name='train_route-list'),
]
