--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.3

-- Started on 2019-09-30 20:05:21

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 24785)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 2911 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 201 (class 1259 OID 24762)
-- Name: cafe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cafe (
    uid uuid NOT NULL,
    name character varying,
    description character varying,
    image character varying,
    price_avg numeric(2,0)
);


ALTER TABLE public.cafe OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24770)
-- Name: case_n_stations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.case_n_stations (
    uid uuid NOT NULL,
    stations uuid,
    cafe uuid
);


ALTER TABLE public.case_n_stations OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24718)
-- Name: stations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stations (
    uid uuid NOT NULL,
    name character varying
);


ALTER TABLE public.stations OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 24726)
-- Name: train; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.train (
    uid uuid NOT NULL,
    name character varying
);


ALTER TABLE public.train OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24745)
-- Name: train_line; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.train_line (
    uid uuid NOT NULL,
    train uuid,
    stations uuid,
    arrival time without time zone,
    departure time without time zone
);


ALTER TABLE public.train_line OWNER TO postgres;

--
-- TOC entry 2904 (class 0 OID 24762)
-- Dependencies: 201
-- Data for Name: cafe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cafe (uid, name, description, image, price_avg) FROM stdin;
\.


--
-- TOC entry 2905 (class 0 OID 24770)
-- Dependencies: 202
-- Data for Name: case_n_stations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.case_n_stations (uid, stations, cafe) FROM stdin;
\.


--
-- TOC entry 2901 (class 0 OID 24718)
-- Dependencies: 198
-- Data for Name: stations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stations (uid, name) FROM stdin;
\.


--
-- TOC entry 2902 (class 0 OID 24726)
-- Dependencies: 199
-- Data for Name: train; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.train (uid, name) FROM stdin;
\.


--
-- TOC entry 2903 (class 0 OID 24745)
-- Dependencies: 200
-- Data for Name: train_line; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.train_line (uid, train, stations, arrival, departure) FROM stdin;
\.


--
-- TOC entry 2772 (class 2606 OID 24769)
-- Name: cafe cafe_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cafe
    ADD CONSTRAINT cafe_pk PRIMARY KEY (uid);


--
-- TOC entry 2774 (class 2606 OID 24784)
-- Name: case_n_stations case_n_stations_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.case_n_stations
    ADD CONSTRAINT case_n_stations_pk PRIMARY KEY (uid);


--
-- TOC entry 2764 (class 2606 OID 24725)
-- Name: stations stations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stations
    ADD CONSTRAINT stations_pkey PRIMARY KEY (uid);


--
-- TOC entry 2770 (class 2606 OID 24749)
-- Name: train_line train_line_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.train_line
    ADD CONSTRAINT train_line_pkey PRIMARY KEY (uid);


--
-- TOC entry 2766 (class 2606 OID 24733)
-- Name: train train_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.train
    ADD CONSTRAINT train_pkey PRIMARY KEY (uid);


--
-- TOC entry 2767 (class 1259 OID 24755)
-- Name: fki_fki_train_line_stations; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_fki_train_line_stations ON public.train_line USING btree (stations);


--
-- TOC entry 2768 (class 1259 OID 24761)
-- Name: fki_fki_train_line_train; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_fki_train_line_train ON public.train_line USING btree (train);


--
-- TOC entry 2777 (class 2606 OID 24773)
-- Name: case_n_stations case_n_stations_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.case_n_stations
    ADD CONSTRAINT case_n_stations_fk FOREIGN KEY (stations) REFERENCES public.stations(uid);


--
-- TOC entry 2778 (class 2606 OID 24778)
-- Name: case_n_stations case_n_stations_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.case_n_stations
    ADD CONSTRAINT case_n_stations_fk_1 FOREIGN KEY (cafe) REFERENCES public.cafe(uid);


--
-- TOC entry 2775 (class 2606 OID 24750)
-- Name: train_line fki_train_line_stations; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.train_line
    ADD CONSTRAINT fki_train_line_stations FOREIGN KEY (stations) REFERENCES public.stations(uid);


--
-- TOC entry 2776 (class 2606 OID 24756)
-- Name: train_line fki_train_line_train; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.train_line
    ADD CONSTRAINT fki_train_line_train FOREIGN KEY (train) REFERENCES public.train(uid);


-- Completed on 2019-09-30 20:05:23

--
-- PostgreSQL database dump complete
--

