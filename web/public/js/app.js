
var app = {

    train_code : 1,
    train : '477',
    date : '29.10.2019',
    rail_veh : '08',
    position : '15',

    get_cafe_menu : function(id_cafe){

        $('.stations-line').html("");
        $.post("/api/menus/"+id_cafe,{},function(data){

        })

        app.render_cafe_menu()
    },

    render_cafe_menu : function (data) {
        var item = {
            food_id : 'e67f3964-34d4-495b-bf8b-447cbf15e0d7',
            food : 'Пицца мясная',
            description : 'Пицца на тонков тесте с большим количеством игридиентов',
            proteins : '454',
            fats : '45',
            carbohydrates : '67',
            kcal : '1454'
        }
        var item2 = {
            food_id : 'e67f7964-34d4-492b-bf8b-447cbf15e0d8',
            food : 'Пицца сырная',
            description : 'Пицца c несколькими видами сыра',
            proteins : '455',
            fats : '85',
            carbohydrates : '98',
            kcal : '1254'
        }

        var menu={
            menu : []
        };
        menu['menu'].push(item)
        menu['menu'].push(item2)

        var source = " <div class='row'>" +
            " {{#each menu}} " +
            "<div class=\"card\" style=\"width: 18rem;\">\n" +
            "            <img src=\"...\" class=\"card-img-top\" alt=\"...\">\n" +
            "            <div class=\"card-body\">\n" +
            "            <p class=\"card-text\"> <h5>{{ food  }}</h5> " +
            "           <i>{{ description }}</i> </p>" +
            "           Белки : {{ proteins }} <br/>" +
            "           Жиры : {{ fats }} <br/> " +
            "           Углеводы : {{ carbohydrates }}  <br/> " +
            "           Ккал : {{ kcal }} " +
            "  <button id='{{food_id}}' class='cart btn btn-primary'><span class='fa fa-shopping-cart'></span> </button> " +
            "        </div>\n" +
            "        </div> " +
            "{{/each}} " +
            "" +
            "" +
            "</div>" +
            "" +
            "" +
            "<script>" +
            "$('.cart').click(function () {\n" +
            "    var food_id = $(this).attr(\"id\");\n" +
            "    app.send_to_card(food_id)\n" +
            "\n" +
            "})" +
            "" +
            "</script>"
        var template = Handlebars.compile(source);
        var content  = template(menu)

        $('.stations-line').html(content);



        console.log(content)

    },

    send_to_card : function (food_id) {
        $.post("/api/order/",{food:food_id},function (data) {

        })
    },

    get_train_cafes : function(param){

        $.ajax({
            url : "http://37.143.9.7:8888/api/cafes/by_train_line/"+app.train_code,
            method : 'get',
            data : {'date':'2019-09-09'},
            success : function(data){
                console.log(data)
                var stations = []
                $.each(data,function (index,item) {
                    //console.log( item)
                    var st = {
                        station_name : item.station_name,
                        delivery_time : item.delivery_time,

                    }
                    var cafe = {
                        name : item.name,
                        description : item.description,
                        logo : item.logo,
                        min_price : item.min_price,
                        id_cafe : item.id
                    }
                    var res = stations.find(function(cur){

                        return (cur.station_name==st.station_name) && (cur.delivery_time==st.delivery_time);
                    })
                    //console.log(res)
                    if (res){
                        res.cafe.push(cafe)
                    } else {
                        st.cafe=[]
                        st.cafe.push(cafe)
                        stations.push(st)
                    }

                    //console.log(cafe,st)
                })
                console.log(stations)

                var rc = $("#stations-line-template").html()
                var t = Handlebars.compile(rc)
                var content  = t({stations:stations})
                console.log(content)
                $('#stations-line').html(content)
            }

        })

    }

}

$('.cart').click(function () {
    var food_id = $(this).attr("id");
    console.log(food_id)

})